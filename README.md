# ACT Fibernet Usage History

Graphs internet usage data from ACT's selfcare portal.

To generate graphs, visit the usual portal at https://selfcare.actcorp.in/ -> My Account. In the "My Usage" page, a new button will be available. This will generate graphs for the selected month.

This extension is an unofficial extension and is in no way affiliated with ACT Fibernet.
