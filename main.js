/*
ACT Fibernet Usage Graph
*/

// Listen for tab changes in the portal
observer = new MutationObserver((mutationsList, observer) => {
  console.log(mutationsList);

  console.log(mutationsList.map((x) => x.target.style.display));

  // Act only when loading the new tab is complete
  var doneLoading = mutationsList.reduce(
    (x, y) => x && y.target.style.display == 'none',
    true
  );
  if (doneLoading) {
    // Reference elements to anchor (and identify tabs)
    const fromTableSearch = $('table').has('td:contains("FROM")');
    const quotaTableSearch = $('table').has(
      'td:contains("Subscribed Package")'
    );

    // If in "My Usage" tab
    if (fromTableSearch.length) {
      // Remove existing graph and re-create button
      $('#graph-this-btn').remove();
      $('.chart-container').remove();
      const $graphBtn = $(
        '<button id="graph-this-btn" style="padding: 6px 20px;font-size: medium;border: none;background: #dbdbdb;border-radius: 5%;margin-left: 10px;">Graph this data!</button>'
      );
      $graphBtn.click(graphThis);
      fromTableSearch.last().before($graphBtn);
    } else if (quotaTableSearch.length) {
      // If in "My Package" tab
      var text = quotaTableSearch
        .last()
        .find('tr')
        .last()
        .find('td')
        .last()
        .text();
      // Get the text and extract the value (considering memory units also)
      const match = /\(Quota ([\d\.]+) (\w+)\)/.exec(text);
      if (match) {
        var quota =
          parseFloat(match[1]) * 1000 ** (memUnits.indexOf(match[2]) - 2);

        // Save quota to extension local storage
        browser.storage.local.set({
          planDetails: { quota: quota },
        });
        planDetails = { quota: quota };
      }
    }
  }
});

// Start observing the loading div for style changes (specifically the display style)
observer.observe(
  document.getElementById(
    '_ACTMyAccount_WAR_ACTMyAccountportlet_:processingPanel'
  ),
  { attributes: true, childList: false, subtree: false }
);

// On button press, generate the graph
function graphThis(event) {
  event.preventDefault();

  // Remove any existing graph
  $('.chart-container').remove();

  // Extract data from usage history table
  const data = $('[id*="myUsageTab_body"]')
    .find('tbody')
    .first()
    .children()
    .toArray()
    .map((x) => {
      return {
        from: new Date(x.children[0].innerText),
        to: new Date(x.children[1].innerText),
        upload: parseFloat(x.children[2].innerText),
        download: parseFloat(x.children[3].innerText),
        total: parseFloat(x.children[4].innerText),
      };
    })
    .reverse();

  // Find cumulative values
  data[0].x = new Date((data[0].from.getTime() + data[0].to.getTime()) / 2);
  for (let i = 1; i < data.length; i++) {
    data[i].x = new Date((data[i].from.getTime() + data[i].to.getTime()) / 2);
    data[i].yDownload = (data[i - 1].yDownload || 0) + data[i].download;
    data[i].yUpload = (data[i - 1].yUpload || 0) + data[i].upload;
    data[i].yTotal = (data[i - 1].yTotal || 0) + data[i].total;
  }

  // This sets the quota level by creating a pseudo-dataset
  var thresholdHighArray = new Array(data.length).fill(planDetails.quota);

  // Add the canvas after the button
  const $canvas =
    '<div class="chart-container" style="position: relative; height:70vh; width:100%; margin-top: 1rem;"><canvas id="myChart" ></canvas></div>';
  $('#graph-this-btn').after($canvas);
  var ctx = document.getElementById('myChart').getContext('2d');

  // The chart object
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      // Use date objects as labels
      labels: data.map((x) => x.x),

      // Only include "Plan Quota" if quota is found in storage
      datasets: (planDetails.quota
        ? [
            {
              label: 'Plan Quota',
              data: thresholdHighArray,
              backgroundColor: 'rgba(0, 0, 0, 0.01)',
              borderColor: 'rgba(0, 0, 0, 0.3)',
              fill: '+1',
              pointRadius: 0,
            },
          ]
        : []
      ).concat([
        {
          label: 'Total',
          data: data.map((x) => x.yTotal),
          backgroundColor: '#ffcd56',
          fill: '+1',
          borderWidth: 1,
        },
        {
          label: 'Download',
          data: data.map((x) => x.yDownload),
          backgroundColor: '#36a2eb',
          fill: '+1',
          borderWidth: 1,
        },
        {
          label: 'Upload',
          data: data.map((x) => x.yUpload),
          backgroundColor: '#ff6384',
          fill: 'origin',
          borderWidth: 1,
        },
      ]),
    },
    options: {
      title: {
        display: true,
        text: planDetails.quota
          ? 'Monthly Usage'
          : 'Visit "My Package" and come back here to view Monthly Quota also',
      },
      scales: {
        xAxes: [
          {
            type: 'time',
            time: {
              unit: 'day',
            },
          },
        ],
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              callback: (value, index, values) => {
                return formatMemSize(value);
              },
            },
          },
        ],
      },
      plugins: {
        filler: {
          propagate: true,
        },
      },
      tooltips: {
        mode: 'index',
        intersect: false,
        callbacks: {
          label: (tooltipItem, data) => {
            let datasetLabel = data.datasets[tooltipItem.datasetIndex].label;
            return datasetLabel + ': ' + formatMemSize(tooltipItem.yLabel);
          },
        },
      },
      hover: {
        mode: 'nearest',
        intersect: true,
      },
    },
  });
}

const memUnits = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

// Format numerical value (in MB) to a better unit
function formatMemSize(value) {
  var x = 0;
  while (value >= 1000) {
    x++;
    value /= 1000;
  }
  value = Math.round(value * 1000) / 1000;
  return `${value} ${memUnits[x + 2]}`;
}

// Clear extension local storage, if needed for some reason
// storage.storage.local.clear();

// Keep a copy of quota in local variable
var planDetails = {};
browser.storage.local.get('planDetails').then(
  (data) => {
    console.log(data);
    planDetails = data.planDetails;
  },
  (err) => console.error(err)
);
